# Voice recognition in virtual reality Unity3D
Voice recognition in virtual reality is a tool that recognizes the command of the user based on a dictionnary provided earlier. The project is developed using Unity3D game engine (c# for the code).

Find a video of the project here : 
(Demo video)[https://gitlab.com/hafsaouajdimp/voice-recognition-in-virtual-reality-unity3d/-/blob/main/voice%20recognition%20in%20VR.mp4?ref_type=heads]
