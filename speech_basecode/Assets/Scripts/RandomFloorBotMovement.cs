using UnityEngine;

public class RandomFloorBotMovement : MonoBehaviour
{
    #region Members
    public Vector2 minimumCoordinates;
    public Vector2 maximumCoordinates;
    public Vector2 speedVariance;
    public Vector2 newDestinationTimeVariance;
    public Vector2 newSpeedTimeVariance;

    protected UnityEngine.AI.NavMeshAgent nav;
    protected float newDestinationTime;
    protected float destinationTimer;
    protected float newSpeedTime;
    protected float speedTimer;
    #endregion

    #region MonoBehaviour callbacks
    protected void Awake ()
    {
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();

        SetRandomDestination();
        SetRandomSpeed();
    }

    protected void Update ()
    {
        destinationTimer += Time.deltaTime;
        speedTimer += Time.deltaTime;

        if(destinationTimer >= newDestinationTime)
        {
            SetRandomDestination();
        }

        if(speedTimer >= newSpeedTime)
        {
            SetRandomSpeed();
        }
    }
    #endregion

    #region Internal methods
    protected void SetRandomDestination ()
    {
        float newX = Random.Range(minimumCoordinates.x, maximumCoordinates.x);
        float newZ = Random.Range(minimumCoordinates.y, maximumCoordinates.y);

        nav.destination = new Vector3(newX, -1f, newZ);

        SetNewDestinationTime();
    }

    protected void SetRandomSpeed ()
    {
        nav.speed = Random.Range(speedVariance.x, speedVariance.y);

        SetNewSpeedTime();
    }

    protected void SetNewDestinationTime ()
    {
        newDestinationTime = Random.Range(newDestinationTimeVariance.x, newDestinationTimeVariance.y);

        destinationTimer = 0f;
    }

    protected void SetNewSpeedTime ()
    {
        newSpeedTime = Random.Range(newSpeedTimeVariance.x, newSpeedTimeVariance.y);

        speedTimer = 0f;
    }
    #endregion
}
