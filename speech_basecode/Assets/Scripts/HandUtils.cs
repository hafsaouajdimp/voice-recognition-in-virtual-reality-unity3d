﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(OVRSkeleton))]
[RequireComponent(typeof(SkinnedMeshRenderer))]
public class HandUtils : MonoBehaviour {

    public enum Hand
    {
        LEFT,
        RIGHT
    }

    public enum Layer
    {
        FLOOR = 6,
        CHARACTER = 7,
    }

    public Hand hand;
    [Range(0.001f, 0.1f)]
    public float pinchThreshold = 0.02f;
    [Range(0.1f, 10.0f)]
	public float rayLength = 1;
	public bool showRay = false;

	protected GameObject ray;
	protected SkinnedMeshRenderer handRenderer;

	public Transform Thumb { get; protected set; }
    public Transform Index { get; protected set; }
	public GameObject Pointed { get; protected set; }
    public Vector3? PointedPosition { get; protected set; }
    public Vector3 RayDirection { get => (hand == Hand.LEFT ? -1 : 1) * Vector3.right; }
    public bool Pinch { get; protected set; }

    protected void Awake() {
		Pinch = false;

        handRenderer = GetComponent<SkinnedMeshRenderer>();

        StartCoroutine(InitHand(GetComponent<OVRSkeleton>()));
	}

    protected void OnDisable()
    {
        Pinch = false;

        ray?.SetActive(false);

        Pointed = null;
        PointedPosition = null;
    }

    protected void Update() {
		bool show_ray = showRay && (handRenderer?.enabled ?? false);

        ray?.SetActive(show_ray);

        // Detect pinch between index and thumb
        if (Thumb != null && Index != null)
        {
            Pinch = (Thumb.transform.position - Index.position).magnitude < pinchThreshold;

            RaycastHit hit;

            if (Physics.Raycast(new Ray(Index.position, Index.TransformDirection(RayDirection)), out hit, rayLength, 1 << (int) Layer.FLOOR | 1 << (int) Layer.CHARACTER))
            {
                Pointed = hit.collider.gameObject;
                PointedPosition = hit.point;
            }
            else
            {
                Pointed = null;
                PointedPosition = null;
            }
        }
	}

	protected IEnumerator InitHand(OVRSkeleton skeleton)
	{
		while(!skeleton.IsInitialized)
		{
			yield return null;
		}

        foreach (OVRBone bone in skeleton.Bones)
        {
            switch (bone.Id)
            {
                case OVRSkeleton.BoneId.Hand_IndexTip:
					Index = bone.Transform;

                    ray = GameObject.CreatePrimitive(PrimitiveType.Cylinder);

                    ray.name = "Ray";
                    ray.transform.parent = Index;
					ray.transform.localPosition = 0.5f * rayLength * RayDirection;
                    ray.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 90.0f);
                    ray.transform.localScale = new Vector3(0.005f, 0.5f * rayLength, 0.005f);

                    Destroy(ray.GetComponent<Collider>());

                    break;
                case OVRSkeleton.BoneId.Hand_ThumbTip:
					Thumb = bone.Transform;

                    break;
            }
        }
    }
}
