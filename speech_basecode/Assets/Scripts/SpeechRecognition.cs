﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;

public class SpeechRecognition : MonoBehaviour
{
    /// <summary>
    /// Class to store the method associated to a command, as well as it's parameters.
    /// </summary>
    public class Command
    {
        public enum Type
        {
            None,
            Move,
            Delete,
            Follow,
            Stop
        }

        #region Members
        public Type action;
        public GameObject actor;
        public object target;
        
        #endregion

        #region Constructors
        // Construct a new command object.
        public Command()
        {
            action = Type.None;
            actor = null;
            target = null;
        }
        
        
        #endregion
        
        #region Overloads
        /// <summary>
        /// Convert the command into a string representation.
        /// </summary>
        /// <returns>The string representing the command values.</returns>
        public override string ToString()
        {
            return string.Format("{0} - {1} - {2}", action, actor?.name, target);
        }

        #endregion
    }

    #region Members
    public HandUtils hand;
    public GameObject[] puppets;
    private float mvtSpeed = 2.0f;

    private GrammarRecognizer recognizer;
    private IReadOnlyDictionary<string, GameObject> match;
    private Text displayText;
    #endregion

    #region MonoBehaviour callbacks
    private void Awake()
    {
        // Check script input parameters
        if(hand == null)
        {
            throw new ArgumentException("Hand must be defined.", "hand");
        }

        if(puppets == null || puppets.Length < 3)
        {
            throw new ArgumentException("Puppets array must be defined and contain at least 3 values.", "puppets");
        }

        // Create a dictionary to simplify the linking between specific words and the associated objects in the scene.
        match = new Dictionary<string, GameObject>
        {
            {"lapin", puppets[0]},
            {"l'éléphant", puppets[1]},
            {"l'ours", puppets[2]},
        };

        // GUI for debugging purposes.
        displayText = GetComponentInChildren<Text>();

		recognizer = new GrammarRecognizer(Application.dataPath + "/Grammar.xml");
        recognizer.OnPhraseRecognized += OnRecognition;
        recognizer.Start();

        // OK, we are ready to start.
        Debug.Log("Ready");
    }

    private void OnDestroy()
    {
        // Close the external speech recognition program on exit.
        if(recognizer != null)
        {
            if(recognizer.IsRunning)
            {
                recognizer.Stop();
            }

            // Close process by sending a close message to its main window.
            recognizer.Dispose();
        }
    }
    #endregion

    #region Internal methods
	/// <summary>
    /// Process a recognized sentence.
    /// </summary>
    /// <param name="args">The parameters of the recognized sentence.</param>
    private void OnRecognition(PhraseRecognizedEventArgs args)
    {
		Debug.LogWarningFormat("{0} - {1}", args.text, args.confidence);
		
        if (args.confidence == ConfidenceLevel.Medium || args.confidence == ConfidenceLevel.High)
        {
			Command cmd = new Command();
			
            // TODO
            
        foreach(string s in args.text.Split(' '))
        {
             HandleRecognition(cmd, s);
        }
            
            ExecCommand(cmd);
            // Update debug display.
            displayText.text = cmd.ToString();
        }
    }

    /// <summary>
    /// Process the current word to update the command parameters.
    /// </summary>
    /// <param name="cmd">The command to update.</param>
    /// <param name="word">The word to analyse.</param>
    private void HandleRecognition(Command cmd, string word)
    {
        
        // Make sure the word is in lower case, to avoid problems of comparing the same word using different cases.
        string w = word.ToLowerInvariant();

        // Check if the word is contained into the object matching dictionary.
        if(match.TryGetValue(w, out GameObject go))
        {
            if (cmd.actor == null)
            {
                cmd.actor = go;
            }
            else if(cmd.actor!=null)
            {
                cmd.target = go;
            }

        }
        else // Check the word manually
        {
            // TODO
            
            if (cmd.action==Command.Type.None)
            {

                if (w == "déplacer")
                {
                    cmd.action = Command.Type.Move;
                    

                }
                else if (w == "détruit")
                {
                    cmd.action = Command.Type.Delete;

                }
                else if (w == "suit")
                {
                    while (cmd.action == Command.Type.Follow)
                    {
                        cmd.action = Command.Type.Follow;
                    }
                    
                    
                }

                else if (w == "arrête")
                {
                    cmd.action = Command.Type.Stop;

                }
                
            }
        }
        if (w == "ici" || w == "là")
        {
            cmd.target = hand.PointedPosition;
            Debug.Log(hand.PointedPosition.Value);
        }
    }
    
	/// <summary>
    /// Execute a command once it's completed.
    /// </summary>
    /// <param name="cmd">Command to execute.</param>
    private void ExecCommand(Command cmd)
    {
        //to DO
       
        if ( cmd.action == Command.Type.Follow)
        {
             GameObject gameObj = (GameObject)cmd.target;
            Move(cmd.actor, gameObj.transform.position);
            cmd.actor.GetComponent<RandomFloorBotMovement>().enabled = false;
            cmd.actor.transform.SetParent(gameObj.transform);
        }
        else if (cmd.action==Command.Type.Delete)
        {  

            Destroy(cmd.actor);
           
        }
        else if (cmd.action == Command.Type.Stop)
        {
            Stop(cmd.actor);
            cmd.actor.transform.SetParent(null);

        }
        if (cmd.action == Command.Type.Move)
        {
            cmd.actor.transform.SetParent(null);

            Vector3 pos= (Vector3) cmd.target;
            Move(cmd.actor, pos);
        }

    }
	
    /// <summary>
    /// Command method to move specified object to the specified destination.
    /// </summary>
    /// <param name="obj">The object to move.</param>
    /// <param name="dest">The destination to move it to.</param>
    private void Move(GameObject obj, Vector3 dest)
    {
        if(obj != null)
        {
            Vector3 move = dest- obj.transform.position;
            obj.transform.Translate(move*Time.deltaTime*mvtSpeed);
            obj.transform.position = dest;
            obj.GetComponent<RandomFloorBotMovement>().enabled = true;
            obj.GetComponent<Animator>().enabled = true;


            Debug.LogFormat("{0} has been moved to {1}", obj.name, dest.ToString("F3"));
        }
    }

    /// <summary>
    /// Command method to destroy the specified object.
    /// </summary>
    /// <param name="obj">The object to destroy.</param>
    private void Delete(GameObject obj)
    {
        if(obj != null)
        {
            obj.SetActive(false);

            Debug.LogFormat("{0} has been deleted", obj.name);
        }
    }
    private void Stop(GameObject g)
    {
         g.GetComponent<RandomFloorBotMovement>().enabled=false;
         g.GetComponent<Animator>().enabled = false;



    }
    #endregion
}
